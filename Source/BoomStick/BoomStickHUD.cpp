// Copyright Epic Games, Inc. All Rights Reserved.

#include "BoomStickHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "BoomStickCharacter.h"

ABoomStickHUD::ABoomStickHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;
}


void ABoomStickHUD::BeginPlay()
{
	Super::BeginPlay();

	if (AmmoWidgetClass)
	{
		AmmoUI = CreateWidget<UAmmoWidget>(GetWorld(), AmmoWidgetClass);
		
		// UI Initialization
		FText PressToLootText = FText::FromString("You are sticked ! Press E to loot !");
		AmmoUI->PressToLoot->SetText(PressToLootText);
		AmmoUI->ShowPressToLoot(false);
		
		if (AmmoUI)
		{
			AmmoUI->AddToViewport();
			ABoomStickCharacter* PlayerCharacter = Cast<ABoomStickCharacter>(GetOwningPawn());
			if (PlayerCharacter)
			{
				// Update Ammo
				AmmoUI->UpdateAmmo(PlayerCharacter->ammo);
			}
		}
	}
}

void ABoomStickHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition((Center.X-8), (Center.Y-8)); // Little fix, the crosshair was at the center.

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}
