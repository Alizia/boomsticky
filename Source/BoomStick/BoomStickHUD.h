// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "AmmoWidget.h"

#include "BoomStickHUD.generated.h"

UCLASS()
class ABoomStickHUD : public AHUD
{
	GENERATED_BODY()

public:
	ABoomStickHUD();

	virtual void BeginPlay() override;

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUserWidget> AmmoWidgetClass;

	UAmmoWidget* AmmoUI;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;
};

