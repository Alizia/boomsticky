// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BoomStickCharacter.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"

#include "BoomStickProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;

UCLASS(config=Game)
class ABoomStickProjectile : public AActor
{
	GENERATED_BODY()

	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
	USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* ProjectileMovement;


public:
	ABoomStickProjectile();

	/** Time before explosion */
	float boomTime;

	/** Timer Rate for BoomTimerHandle */
	float boomTimerRate;

	/** Delay between color change */
	float currentBlinkRate;

	/** is sticked on a player */
	bool isSticky;

	/** Reference to the player sticked */
	ABoomStickCharacter* stickyOnRef;

	/** explosion FX */
	UPROPERTY(EditDefaultsOnly, Category=Effect)
	UParticleSystem* ExplosionFX;

	/** Generate tick for the bomb */
	FTimerHandle BoomTimerHandle;

	/** Change color regulary */
	FTimerHandle ChangeColorHandle;

	/** Called when projectile hits something, ignored if from client */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/* Manage the hit serverside */
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	bool Server_OnHit_Validate(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	void Server_OnHit_Implementation(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/* Update hit information clientside */
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Multi_OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	bool Multi_OnHit_Validate(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	void Multi_OnHit_Implementation(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	USphereComponent* GetCollisionComp() const { return CollisionComp; }

	/** Returns ProjectileMovement subobject **/
	UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	/** Manage the explosion timer */
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_TickTack();
	bool Server_TickTack_Validate();
	void Server_TickTack_Implementation();

	/** Change the projectile color */
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Multi_ChangeColor();
	bool Multi_ChangeColor_Validate();
	void Multi_ChangeColor_Implementation();

	/** Spawn the explosion emitter and then destroy */
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Multi_OnExplosion();
	bool Multi_OnExplosion_Validate();
	void Multi_OnExplosion_Implementation();

	/** Just destroy */
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Multi_OnDestruction();
	bool Multi_OnDestruction_Validate();
	void Multi_OnDestruction_Implementation();
};

