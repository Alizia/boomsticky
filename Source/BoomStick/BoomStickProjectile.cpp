// Copyright Epic Games, Inc. All Rights Reserved.

#include "BoomStickProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"

ABoomStickProjectile::ABoomStickProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ABoomStickProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;
	ProjectileMovement->ProjectileGravityScale = 0;

	InitialLifeSpan = 0;

	boomTime = 8.0f;
	boomTimerRate = 0.1f;
	currentBlinkRate = 0;
	isSticky = false;

	SetReplicates(true);

	if (HasAuthority())
	{
		UWorld* world = GetWorld();
		if (world) // Avoid UE Crash at launch
		{
			world->GetTimerManager().SetTimer(BoomTimerHandle, this, &ABoomStickProjectile::Server_TickTack, boomTimerRate, true);
		}
	}
}

void ABoomStickProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!isSticky)
	{
		if (HasAuthority())
		{
			ABoomStickProjectile::Server_OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
		}
	}
}

bool ABoomStickProjectile::Server_OnHit_Validate(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	return true;
}

void ABoomStickProjectile::Server_OnHit_Implementation(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ABoomStickCharacter* PlayerChar = Cast<ABoomStickCharacter>(OtherActor);
	if (PlayerChar)
	{
		AttachToComponent(PlayerChar->GetRootComponent(), FAttachmentTransformRules::KeepWorldTransform, Hit.BoneName); // Stick the projectile
		PlayerChar->Server_OnAddStickyProjectile(this); // Add this projectile to the player list

		stickyOnRef = PlayerChar;
		boomTime = 4.0f; // 4 sec before explosion if sticky

		ABoomStickProjectile::Multi_OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit); // Update client information
	}
}

bool ABoomStickProjectile::Multi_OnHit_Validate(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	return true;
}

void ABoomStickProjectile::Multi_OnHit_Implementation(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ABoomStickCharacter* PlayerChar = Cast<ABoomStickCharacter>(OtherActor);
	if (PlayerChar)
	{
		isSticky = true;
		PlayerChar->MoveIgnoreActorAdd(this); // Prevents the player from becoming Superman
		GetProjectileMovement()->StopMovementImmediately(); // Sticky projectile are sticked
	}
}

bool ABoomStickProjectile::Server_TickTack_Validate()
{
	return true;
}

void ABoomStickProjectile::Server_TickTack_Implementation()
{
	boomTime -= boomTimerRate;

	if (boomTime <= 0) // Explosion
	{
		
		if (isSticky)
		{
			// The projectile is sticked on a player
			stickyOnRef->Server_OnDelStickyProjectile(this);
		}
		else
		{
			// The projectile is not sticked on a player, just explode
			Multi_OnExplosion();
		}
	}
	else if (boomTime <= 1) // Quick blink
	{
		if (currentBlinkRate != 0.1f)
		{
			currentBlinkRate = 0.1f;
			GetWorldTimerManager().SetTimer(ChangeColorHandle, this, &ABoomStickProjectile::Multi_ChangeColor, currentBlinkRate, true);
		}
	}
	else if (boomTime <= 2) // Medium blink
	{
		if (currentBlinkRate != 0.2f)
		{
			currentBlinkRate = 0.2f;
			GetWorldTimerManager().SetTimer(ChangeColorHandle, this, &ABoomStickProjectile::Multi_ChangeColor, currentBlinkRate, true);
		}
	}
	else if (boomTime <= 3) // Slow blink
	{
		if (currentBlinkRate != 0.5f)
		{
			currentBlinkRate = 0.5f;
			GetWorldTimerManager().SetTimer(ChangeColorHandle, this, &ABoomStickProjectile::Multi_ChangeColor, currentBlinkRate, true);
		}
	}
}

bool ABoomStickProjectile::Multi_ChangeColor_Validate()
{
	return true;
}

void ABoomStickProjectile::Multi_ChangeColor_Implementation()
{	
	UStaticMeshComponent* SMC = FindComponentByClass<UStaticMeshComponent>();
	if (SMC)
	{
		UMaterialInstanceDynamic* MID = SMC->CreateAndSetMaterialInstanceDynamic(0);
		if (MID)
		{
			FLinearColor currentColor;

			if (MID->GetVectorParameterValue(FName(TEXT("DiffuseColor")), currentColor)) // Just toggle the color between Red/Yellow at each call
			{
				if (currentColor == FLinearColor::Red)
				{
					MID->SetVectorParameterValue(FName(TEXT("DiffuseColor")), FLinearColor::Yellow);
				}
				else
				{
					MID->SetVectorParameterValue(FName(TEXT("DiffuseColor")), FLinearColor::Red);
				}
	
				SMC->SetMaterial(0, MID);
			}
		}
	}
}

bool ABoomStickProjectile::Multi_OnExplosion_Validate()
{
	return true;
}

void ABoomStickProjectile::Multi_OnExplosion_Implementation()
{

	if (ExplosionFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionFX, GetActorLocation(), GetActorRotation());
		Destroy();
	}
}

bool ABoomStickProjectile::Multi_OnDestruction_Validate()
{
	return true;
}

void ABoomStickProjectile::Multi_OnDestruction_Implementation()
{
	Destroy();
}
