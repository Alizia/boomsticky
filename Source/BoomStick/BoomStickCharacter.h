// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BoomStickHUD.h"
#include "Net/UnrealNetwork.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"

#include "BoomStickCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UMotionControllerComponent;
class UAnimMontage;
class USoundBase;

UCLASS(config=Game)
class ABoomStickCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FP_Gun;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USceneComponent* FP_MuzzleLocation;

	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* VR_Gun;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMotionControllerComponent* L_MotionController;

public:
	ABoomStickCharacter();

protected:
	virtual void BeginPlay();

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class ABoomStickProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	UAnimMontage* FireAnimation;

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint8 bUsingMotionControllers : 1;

	/** Player ammo */
	UPROPERTY(Replicated, BlueprintReadOnly, Category="Weapon")
	int ammo;

	/* List of the projectile sticked on the player */
	TArray<ABoomStickProjectile*> StickyProjectileList;

	/* Can the player loot a projectile */
	UPROPERTY(Replicated, BlueprintReadOnly, Category="Weapon")
	bool canLoot;

	/* Update the ammo counter UI */
	UFUNCTION(Client, Reliable, WithValidation)
	void Client_OnAmmoUpdate(int newAmmoNumber);
	bool Client_OnAmmoUpdate_Validate(int newAmmoNumber);
	void Client_OnAmmoUpdate_Implementation(int newAmmoNumber);

	/* Add the projectile to the player list */
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnAddStickyProjectile(ABoomStickProjectile* StickyProjectile);
	bool Server_OnAddStickyProjectile_Validate(ABoomStickProjectile* StickyProjectile);
	void Server_OnAddStickyProjectile_Implementation(ABoomStickProjectile* StickyProjectile);

	/* Remove the projectile to the player list */
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnDelStickyProjectile(ABoomStickProjectile* StickyProjectile);
	bool Server_OnDelStickyProjectile_Validate(ABoomStickProjectile* StickyProjectile);
	void Server_OnDelStickyProjectile_Implementation(ABoomStickProjectile* StickyProjectile);

	/* Update the 'Press E to loot' message */
	UFUNCTION(Client, Reliable, WithValidation)
	void Client_OnCanLootUpdate(bool newCanLoot);
	bool Client_OnCanLootUpdate_Validate(bool newCanLoot);
	void Client_OnCanLootUpdate_Implementation(bool newCanLoot);

protected:
	
	/** The client want to shot */
	void OnFire();

	/* Manage the fire action serverside */
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnFire();
	bool Server_OnFire_Validate();
	void Server_OnFire_Implementation();

	/* Replicate shot sound to clients */
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Multi_OnFire();
	bool Multi_OnFire_Validate();
	void Multi_OnFire_Implementation();

	/** The client want to loot */
	void OnLoot();

	/* Manage the loot action serverside */
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnLoot();
	bool Server_OnLoot_Validate();
	void Server_OnLoot_Implementation();

	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	TouchData	TouchItem;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

public:
	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

};

