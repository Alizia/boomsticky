// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"

#include "AmmoWidget.generated.h"

/**
 * 
 */
UCLASS()
class BOOMSTICK_API UAmmoWidget : public UUserWidget
{
	GENERATED_BODY()
	
	UPROPERTY(meta=(BindWidget))
		UTextBlock* AmmoNumber;

public:
	UPROPERTY(meta=(BindWidget))
		UTextBlock* PressToLoot;

	/* Update the Ammo counter */
	void UpdateAmmo(int number);
	/* Toggle the 'Press E to loot' message */
	void ShowPressToLoot(bool visibily);
};
