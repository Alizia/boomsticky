// Copyright Epic Games, Inc. All Rights Reserved.

#include "BoomStickGameMode.h"
#include "BoomStickHUD.h"
#include "BoomStickCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABoomStickGameMode::ABoomStickGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ABoomStickHUD::StaticClass();
}
