// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BoomStickGameMode.generated.h"

UCLASS(minimalapi)
class ABoomStickGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABoomStickGameMode();
};



