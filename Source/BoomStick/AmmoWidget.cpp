// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoWidget.h"

void UAmmoWidget::UpdateAmmo(int number)
{
	FString AmmoStr = FString::Printf(TEXT("Ammo = %i"), number);
	FText AmmoTxt = FText::FromString(AmmoStr);

	AmmoNumber->SetText(AmmoTxt);
}

void UAmmoWidget::ShowPressToLoot(bool visibily)
{
	if (visibily)
	{
		PressToLoot->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		PressToLoot->SetVisibility(ESlateVisibility::Hidden);
	}
}
