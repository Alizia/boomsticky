// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class BoomStick : ModuleRules
{
	public BoomStick(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
